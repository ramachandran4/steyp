import Image from 'next/image'
import React from 'react'

export const cardData = [
    {
        title: 'UI Engineer',
        description: 'Develop an interactive responsive website using HTML, CSS, JavaScript and its modern Frameworks.',
        imgSrc: '/assets/images/ui-engineer.jpg',
        imgAlt: 'ui'
    },
    {
        title: 'Backend Developer',
        description: 'Build beautiful websites with CMS features using Python and Django web framework',
        imgSrc: '/assets/images/backend-developer.jpg',
        imgAlt: 'backend'
    },
    {
        title: 'DevOps Engineer',
        description: 'Build, publish and maintain web application',
        imgSrc: '/assets/images/devops-engineer.jpg',
        imgAlt: 'devops'
    },
    {
        title: 'Web Application Developer',
        description: 'Build an entire Web application with Frontend, Backend and API connections.',
        imgSrc: '/assets/images/web-application.jpg',
        imgAlt: 'web application'
    },
    {
        title: 'Mobile Application Developer',
        description: 'Creating iOS and Android Applications and publishing on respective platform',
        imgSrc: '/assets/images/mobile-application.jpg',
        imgAlt: 'mobile application'
    },
];

const Syllabus = () => {
    return (
        <div className='py-20'>
            <div className='wrapper'>
                <div>
                    <div className='relative mb-12'>
                        <h2 className='text-[32px] OpenSans_SemiBold text-center mb-5'><span className='text-[#0e9f6a]'>Our</span> Syllabus</h2>
                        <div className='w-32 absolute left-[48%] top-[30%]'>
                            <Image src={require("../../../public/assets/images/green-line.svg").default} width={95} height={45} alt='line' />
                        </div>
                        <p className='text-center text-[#545454] text-base'>To mould highly skilled engineering professionals through comprehensive <br /> technology training and mentorship.</p>
                    </div>
                    <div className='flex flex-wrap w-[90%] justify-center m-auto gap-5'>
                        {cardData.map((card, index) => (
                            <div key={index} className='w-[30%] py-5 px-5 bg-[#f8f8f8] rounded-lg'>
                                <div className='w-full mb-4'>
                                    <Image className='rounded-lg' src={card.imgSrc} width={1000} height={1000} alt={card.imgAlt} />
                                </div>
                                <h3 className='text-[#48445b] OpenSans_SemiBold text-base mb-2'>{card.title}</h3>
                                <p className='text-[#707070] text-sm'>{card.description}</p>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Syllabus
