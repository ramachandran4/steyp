"use client";

import Image from 'next/image'
import React from 'react'

const features = [
    {
        id: 1,
        imageSrc: require("../../../public/assets/images/01.svg").default,
        title: 'Systematic Learning',
        description: '365 Days syllabus for students',
    },
    {
        id: 2,
        imageSrc: require("../../../public/assets/images/02.svg").default,
        title: 'Creating Engineers',
        description: 'Learn from the basics and become an expert in the field.',
    },
    {
        id: 3,
        imageSrc: require("../../../public/assets/images/03.svg").default,
        title: 'Building Future Scientists',
        description: 'Moulding to identify and solve future challenges',
    },
];

const Program = () => {
    return (
        <div className='py-20'>
            <div className='wrapper'>
                <div className='flex justify-between w-full relative pb-[82px]'>
                    <div className='w-[45%]'>
                        <h2 className='text-[32px] OpenSans_SemiBold text-black mb-8'>Engineering Program <span className='text-[#0e9f6a]'>for School Students</span></h2>
                        <p className='text-[#545454]'>Steyp's engineering program is a comprehensive training program for school students from 5th to 12th class, to mould them into skilled engineers and future scientists.</p>
                        <br />
                        <p className='text-[#545454] mb-8'>In the 365 days program, the student will be trained from zero to become a professional engineer.</p>
                        <button className='py-3 px-6 start rounded-lg'><a href="#" className='OpenSans_Bold text-white'>Start free trail!</a></button>
                        <div className='w-16 absolute left-[13%] top-[80%]'>
                            <Image src={require("../../../public/assets/images/right-arrow.svg")} width={95} height={44} alt='arrow' />
                        </div>
                    </div>
                    <div className='w-[45%] relative'>
                        <div className='w-full absolute top-[-15%]'>
                            <Image src={require("../../../public/assets/images/sat-mamooty.png")} width={1000} height={1000} alt='mamooty' />
                        </div>
                        {features.map((feature, index) => (
                            <div
                                key={feature.id}
                                className={`py-7 px-7 w-72 bg-white rounded-lg absolute shadow-lg ${index === 0 ? 'top-[70%] left-[-16%]' : index === 1 ? 'top-[70%] left-[52%]' : 'top-[118%] left-[18%]'}`}
                                style={{ zIndex: index === 1 ? 1 : 0 }}
                            >
                                <div className='w-16'>
                                    <Image src={feature.imageSrc} width={100} height={100} alt={`feature-${feature.id}`} />
                                </div>
                                <h3 className='mb-2 OpenSans_SemiBold'>{feature.title}</h3>
                                <p className='text-[16px] text-[#545454]'>{feature.description}</p>
                                {index === 0 && (
                                    <div className='w-24 absolute top-[-20%] right-[85%]'>
                                        <Image src={require("../../../public/assets/images/yellow-deco.svg").default} width={100} height={100} alt='decoration' />
                                    </div>
                                )}
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Program