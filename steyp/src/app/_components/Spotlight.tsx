"use client";

import Image from 'next/image'
import React, { useState } from 'react'

// import PhoneInput from 'react-phone-input-2';
// import 'react-phone-input-2/lib/style.css'

const Spotlight = () => {

    // const [phone, setPhone] = useState("")

    return (
        <div className='py-14'>
            <div className='wrapper'>
                <div className='bg-[#f7f6f2d9] px-20 rounded-lg'>
                    <div className='flex justify-between w-full pt-20 pb-20 items-center'>
                        <div className='w-[45%]'>
                            <h2 className='text-[43px] OpenSans_Bold text-[#2d2d2d] mb-5'>Creating Computer <span className='text-[#0e9f6a]'>Engineers</span> & Tech <br /> <span className='text-[#0e9f6a]'>Scientists</span></h2>
                            <p className='text-lg text-[#707070] mb-8'>Steyp is a Digital University for students to learn and become Computer Engineers and Tech Scientists irrespective of their age or educational background.</p>
                            <h4 className='text-lg OpenSans_Bold text-[#545454]'>Get your free trial account now!</h4>
                            <div className='flex gap-2 mb-12'>
                                <div className='flex py-1 px-1 bg-white rounded-lg w-[65%]'>
                                    <div className='flex items-center'>    
                                        <div className='w-10 h-[40px]'>
                                            <Image className='rounded-full' src={require("../../../public/assets/images/flag.png")} width={95} height={45} alt='flag' />
                                        </div>
                                        <div className='w-6'>
                                            <Image src={require("../../../public/assets/images/expand.svg").default} width={95} height={45} alt='expand' />
                                        </div>
                                    </div>
                                    <div className='flex items-center gap-2 py-3 pl-0 pr-4 rounded-lg'>
                                        <h5 className='text-sm OpenSans_Bold'>+91</h5>
                                        <input type="tel" id='phone' placeholder='Enter your phone number' maxLength={10} className='w-full text-base OpenSans_Medium'/>
                                    </div>
                                </div>
                                {/* <PhoneInput
                                    inputStyle={{ height: "60px", width: "100%" }}
                                    placeholder='Phone number'
                                    value={phone}
                                    onChange={() => setPhone(phone)}
                                /> */}
                                <button className='py-1 px-10 start rounded-lg'><a href="#" className='OpenSans_Bold text-white'>Start now!</a></button>
                            </div>
                            <div>
                                <div className='flex items-center gap-2 mb-5'>
                                    <div className='w-8'>
                                        <Image src={require("../../../public/assets/images/not.svg").default} width={45} height={45} alt='not' />
                                    </div>
                                    <h3 className='text-[#545454]'>Try free for <b className='text-[#707070] OpenSans_SemiBold'>5 days</b>.No credit card needed</h3>
                                </div>
                                <div className='flex items-center gap-2 mb-5'>
                                    <div className='w-8'>
                                        <Image src={require("../../../public/assets/images/not.svg").default} width={45} height={45} alt='not' />
                                    </div>
                                    <h3 className='text-[#545454]'>By signing up you agree to the <a href="#" className='underline OpenSans_SemiBold  text-[#707070]'>Terms</a> and <a href="#" className='underline OpenSans_SemiBold  text-[#707070]'>Privacy policy</a></h3>
                                </div>
                                <div className='flex items-center gap-2'>
                                    <div className='w-8'>
                                        <Image src={require("../../../public/assets/images/not.svg").default} width={45} height={45} alt='not' />
                                    </div>
                                    <h3 className='text-[#545454]'>View our <a href="#" className='underline OpenSans_SemiBold text-[#707070]'>Subscription Plans</a></h3>
                                </div>
                            </div>
                        </div>
                        <div className='w-[50%] relative'>
                            <div className='w-full'>
                                <Image src={require("../../../public/assets/images/newhero.png")} width={1000} height={1000} alt='hero' />
                            </div>
                            <div className='w-[16%] absolute top-36 left-56 cursor-pointer'>
                                <Image src={require("../../../public/assets/images/orangeplay.svg").default} width={95} height={50} alt='hero' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Spotlight