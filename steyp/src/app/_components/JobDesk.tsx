import Image from 'next/image'
import React from 'react'

const JobDesk = () => {
    return (
        <div className='py-32 bg-[#edf9fe]'>
            <div className='wrapper'>
                <div className='flex justify-between w-full'>
                    <div className='w-[45%]'>
                        <div className='bg-[#84b988] pt-7 pb-0 px-7 mr-20 rounded-lg'>
                            <div className='flex gap-5 py-4 px-4 bg-white items-start rounded-lg mb-6'>
                                <div className='w-16'>
                                    <Image src={require("../../../public/assets/images/interview.svg").default} width={100} height={100} alt='interview' />
                                </div>
                                <div>
                                    <h3 className='text-base OpenSans_SemiBold mb-1'>One-year online internship</h3>
                                    <p className='text-[#545454]'>Industrial engineering experience while pursing academics.</p>
                                </div>
                            </div>
                            <div className='flex gap-5 py-4 px-4 bg-white items-start rounded-lg mb-6'>
                                <div className='w-28'>
                                    <Image src={require("../../../public/assets/images/training.svg").default} width={100} height={100} alt='training' />
                                </div>
                                <div>
                                    <h3 className='text-base OpenSans_SemiBold mb-1'>Full Stack Development</h3>
                                    <p className='text-[#545454]'>Learning website development, web application development and mobile application development.</p>
                                </div>
                            </div>
                            <div className='flex gap-5 py-4 px-4 bg-white items-start rounded-lg translate-x-24 shadow-md relative'>
                                <div className='w-14'>
                                    <Image src={require("../../../public/assets/images/job.svg").default} width={100} height={100} alt='job' />
                                </div>
                                <div>
                                    <h3 className='text-base OpenSans_SemiBold mb-1'>Spend 1 hr daily and be an <br />engineer</h3>
                                    <p className='text-[#545454]'>Syllabus arranged one hour each for 365 Days.</p>
                                </div>
                                <div className='w-16 absolute top-[95%] left-[95%]'>
                                    <Image src={require("../../../public/assets/images/right-arrow.svg")} width={95} height={44} alt='arrow' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='w-[45%]'>
                        <h2 className='text-[32px] OpenSans_SemiBold text-black mb-8'>An Engineering Program <span className='text-[#0e9f6a]'>for <br /> College Students</span></h2>
                        <p className='text-[#545454]'>Steyp's engineering programme for college students aims to prepare them to become industry-ready computer engineers with excellent technical skills.</p>
                        <br />
                        <p className='text-[#545454] mb-8'>This programme allows students to gain industry experience while pursuing their degree.</p>
                        <button className='py-3 px-6 start rounded-lg'><a href="#" className='OpenSans_Bold text-white'>Start free trail!</a></button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default JobDesk
