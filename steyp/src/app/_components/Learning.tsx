"use client";

import Image from 'next/image'
import React from 'react'

const steps = [
	{
		id: 1,
		bgColor: '#edf9ff',
		image: 'blue-tick.png',
		title: 'Systematic E-Learning Syllabus',
		description: 'Spend One hour per day for 365 days.'
	},
	{
		id: 2,
		bgColor: '#fff5e0',
		image: 'yellow-tick.png',
		title: 'Practice',
		description: 'A timely practice session to get experience with the real world projects.'
	},
	{
		id: 3,
		bgColor: '#fff4f3',
		image: 'red-tick.png',
		title: 'Workshop',
		description: 'Build a better understanding on practice session through workshop.'
	},
	{
		id: 4,
		bgColor: '#e9f7f2',
		image: 'green-tick.png',
		title: 'Assessment',
		description: 'Evaluate and know where you stand with regular assessments.'
	},
	{
		id: 5,
		bgColor: '#fff4f3',
		image: 'red-tick.png',
		title: 'Support',
		description: 'Support for doubts and queries, because asking is learning.'
	},
	{
		id: 6,
		bgColor: '#e9f7f2',
		image: 'green-tick.png',
		title: 'Follow up',
		description: 'Assistance by dedicated Students Relationship Managers.'
	},
	{
		id: 7,
		bgColor: '#fff4f3',
		image: 'red-tick.png',
		title: 'Hackathon',
		description: 'Exclusive hackathons and coding events for Steyp students.'
	},
	{
		id: 8,
		bgColor: '#fff5e0',
		image: 'yellow-tick.png',
		title: 'Certification',
		description: 'Earn certificates on successfully completing a profession.'
	},
	{
		id: 9,
		bgColor: '#edf9ff',
		image: 'blue-tick.png',
		title: 'Placement',
		description: 'Opportunity for placement on successful completion of the program.'
	},
];

const Learning = () => {
	return (
		<div className='py-32 bg-[#fffaf0]'>
			<div className='wrapper'>
				<h2 className='text-[32px] OpenSans_SemiBold text-black text-center mb-3'><span className='text-[#0e9f6a]'>Engineering</span> Program Learning Process</h2>
				<p className='text-[#545454] text-center'>To mould highly skilled engineering professionals through comprehensive <br /> technology training and mentorship</p>
				<div className='flex flex-wrap w-full justify-between pt-10 gap-5'>
					{steps.map((step) => (
						<div key={step.id} className='w-[32%] flex gap-5 py-5 px-5 bg-white rounded-lg'>
							<div className=''>
								<div className={`w-10 py-2 px-2 rounded-lg`} style={{ backgroundColor: step.bgColor }}>
									<Image
										src={require(`../../../public/assets/images/${step.image}`).default}
										width={100}
										height={100}
										alt='tick'
									/>
								</div>
							</div>
							<div>
								<h3 className='text-lg OpenSans_Bold text-[#545454] mb-3'>{step.title}</h3>
								<p className='text-[#707070] text-[16px]'>{step.description}</p>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	)
}

export default Learning

