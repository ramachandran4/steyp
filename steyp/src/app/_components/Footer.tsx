import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const Footer = () => {
    return (
        <div className='pt-32 pb-8 bg-[#1e1e1e]'>
            <div className='wrapper'>
                <div className='flex justify-between w-full'>
                    <div className='w-[45%]'>
                        <div className='w-[20%] mb-5 grayscale-[1]'>
                            <a href="#"><Image src={require("../../../public/assets/images/steyp-logo-new.svg").default} width={50} height={50} alt='steyp' /></a>
                        </div>
                        <p className='text-[#aaaaaa]'>Steyp Private Limited,<br />#208, 2nd Floor,<br />HiLITE Platino,<br />Shankar Nagar Road,Maradu,<br />Kakkanad, Kerala,<br />India - 682304</p>
                    </div>
                    <div className='w-[55%] flex'>
                        <div className='w-[55%]'>
                            <h5 className='mb-5 text-xl OpenSans_SemiBold text-white'>Company</h5>
                            <div className='mb-2'>
                                <Link href="#" className='text-[#aaaaaa]'>About Us</Link>
                            </div>
                           <div className='mb-2'>
                                <Link href="#" className='text-[#aaaaaa]'>Contact Us</Link>
                           </div>
                            <div>
                                <Link href="#" className='text-[#aaaaaa]'>Success Stories</Link>
                            </div>
                        </div>
                        <div className='w-[55%]'>
                            <h5 className='mb-5 text-xl OpenSans_SemiBold text-white'>Contact</h5>
                            <div className='mb-2'>
                                <Link href="#" className='text-[#aaaaaa] mb-2'>+91 9778329954</Link>
                            </div>
                            <div className='mb-5'>
                                <Link href="hello@steyp.com" className='text-[#aaaaaa]'>hello@steyp.com</Link>
                            </div>
                            <div className='w-[55%] flex gap-5 justify-between'>
                                <Link href="http://instagram.com"><Image src={require("../../../public/assets/images/instagram-color.svg").default} width={50} height={50} alt='instagrm' /></Link>
                                <Link href="http://facebook.com"><Image src={require("../../../public/assets/images/facebook.svg").default} width={50} height={50} alt='facebook' /></Link>
                                <Link href="http://twitterx.com"><Image src={require("../../../public/assets/images/twitterx.svg").default} width={50} height={50} alt='twitter' /></Link>
                                <Link href="http://linkdin.com"><Image src={require("../../../public/assets/images/linkedin.svg").default} width={50} height={50} alt='linkedin' /></Link>
                                <Link href="http://youtude.com"><Image src={require("../../../public/assets/images/youtube-color.svg").default} width={50} height={50} alt='youtube' /></Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='flex justify-between items-center my-16'>
                    <div>
                        <Link href="#" className='text-[#aaaaaa]'>© 2024, Steyp Private Limited. All rights reserved</Link>
                    </div>
                    <div className='flex gap-5'>
                        <div>
                            <Link href="#" className='text-[#aaaaaa]'>Terms of Service</Link>
                        </div>
                        <div>
                            <Link href="#" className='text-[#aaaaaa]'>Privacy Policy</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
