import Image from 'next/image'
import React from 'react'

const Banner = () => {
    return (
        <div className='py-32'>
            <div className='wrapper'>
                <div className='banner flex justify-between w-full rounded-lg'>
                    <div className='flex justify-between w-[90%] relative'>
                        <div className='mx-40 pt-24 pb-36'>
                            <h4 className='text-3xl OpenSans_SemiBold text-white mb-2'>Our <span className='text-[#0e9f6a]'>Brand Ambassador</span></h4>
                            <div className='w-[50%] flex items-center gap-5'>
                                <h3 className='text-3xl OpenSans_Regular text-white'>Megastar</h3>
                                <Image src={require("../../../public/assets/images/mammootty.png")} width={1000} height={1000} alt='mammotty' />
                            </div>
                            <div className='w-[23%] absolute left-[27%] top-[53%]'>
                                <Image src={require("../../../public/assets/images/line.svg").default} width={50} height={50} alt='line' />
                            </div>
                            <div className='w-[15%] absolute left-0 top-[45%]'>
                                <Image src={require("../../../public/assets/images/round.svg").default} width={50} height={50} alt='line' />
                            </div>
                        </div>
                        <div>
                            <div className='absolute w-[28%] left-[77%] top-[-30%]'>
                                <Image src={require("../../../public/assets/images/banner.png")} width={1000} height={1000} alt='banner' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Banner
