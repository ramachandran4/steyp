"use client";

import React from 'react'
import LoginPage from './auth/_compontents/LoginPage'
import Header from '../components/include/Header'
import Head from 'next/head'
import Spotlight from './_components/Spotlight'
import Program from './_components/Program'
import JobDesk from './_components/JobDesk'
import Learning from './_components/Learning'
import Banner from './_components/Banner'
import Syllabus from './_components/Syllabus'
import Footer from './_components/Footer'

const page = () => {
	return (
		<>
			<Head>
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<Header />
			<LoginPage />
			<Spotlight />
			<Program />
			<JobDesk />
			<Learning />
			<Banner />
			<Syllabus />
			<Footer />	
		</>
	)
}

export default page
