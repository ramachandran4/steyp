"use client"
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'

const Password = () => {
    const [PasswordModal, setPasswordModal] = useState(true);
    const handleClose = () => {
        setPasswordModal(false);
    };
    const [value, setValue] = useState()

    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);
  
    const handleTogglePassword = () => {
      setShowPassword((prevShowPassword) => !prevShowPassword);
    };
    return (
        <>
            {PasswordModal && (
                <div className='inset-0 flex items-center justify-end fixed bg-black bg-opacity-70'>
                    <div className='w-10 flex justify-end absolute top-8 left-[56%]'>
                        <a href="#" onClick={handleClose}>
                            <Image src={require("../../../../public/assets/images/cancel.svg").default} width={96} height={45} alt='cancel' />
                        </a>
                    </div>
                    <div className='h-full'
                        style={{
                            animation: "slideIn 0.5s forwards",
                            transformOrigin: "right",
                            textAlign: "left",
                        }}>
                        <div className='decorator bg-white shadow-md pt-52 px-24 h-full'>
                            <h4 className='text-3xl OpenSans_Bold text-gray-800 mb-3'>Password</h4>
                            <p className='text-[#545454] OpenSans_Regular mt-3 mb-10'>Enter your password for this account</p>
                            <div className='flex items-center mb-14'>
                                <div className='flex items-center gap-2 border-[1px] border-solid border-[#5cc66a] py-3 px-4 w-full rounded-lg'>
                                    <div className='w-4'>
                                        <Image src={require("../../../../public/assets/images/lock.svg").default} width={100} height={65} alt='lock' />
                                    </div>
                                    <input type={showPassword ? 'text' : 'password'} value={password} onChange={(e) => setPassword(e.target.value)} id='password' placeholder='Enter your password' maxLength={16} className='w-full'/>
                                    {/* <button type="button" onClick={handleTogglePassword}>
                                        {showPassword ? 'Hide' : 'Show'} Password
                                    </button> */}
                                    {/* <div className='w-5'>
                                        <Image src={showPassword ? '/hide.svg' : '/eye.svg'}
                                            alt={showPassword ? 'Hide password' : 'Show password'}
                                            onClick={handleTogglePassword}
                                            style={{ cursor: 'pointer' }} src={require("../../../../public/assets/images/eye.svg").default} width={100} height={65} alt='hide' />
                                    </div> */}
                                </div>
                            </div>
                            <div className='absolute top-[47%] left-[86%]'>
                                <Link href="#" className='text-sm OpenSans_Bold'>Login with OTP</Link>
                            </div>
                            <Link href="#" className='py-3 px-48 bg-green-400 text-xl OpenSans_Bold text-white rounded-lg'>Login</Link>
                            <div className='flex text-center justify-center my-8 gap-2 mb-36'>
                                <Link href="#" className='OpenSans_Bold text-[#5cc66ad9]'>Forget Password?</Link>
                            </div>
                            <hr className='border-solid border-[#a5a5a5] mb-5'/>
                            <div className='text-center'>
                                <Link href="#" className='OpenSans_Medium text-gray-800'>Terms of service</Link>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

export default Password
