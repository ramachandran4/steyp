"use client"
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'
import Password from './Password'
// import Password from '@/app/auth/_compontents/Password'

const LoginPage = () => {
    const [LoginModal, setLoginModal] = useState(true);
    const [isPasswordModal, setIsPasswordModal] = useState(false);
    const handleClose = () => {
        setLoginModal(false);
    };
    const [value, setValue] = useState()
    return (
        <>
            {LoginModal && (
                <div className='inset-0 flex items-center justify-end fixed bg-black bg-opacity-70 z-[1]'>
                    <div className='w-10 flex justify-end absolute top-8 left-[56%]' onClick={handleClose}>
                        <a href="#" onClick={handleClose}>
                            <Image src={require("../../../../public/assets/images/cancel.svg").default} width={96} height={45} alt='cancel' />
                        </a>
                    </div>
                    <div className='h-full'
                        style={{
                            animation: "slideIn 0.5s forwards",
                            transformOrigin: "right",
                            textAlign: "left",
                        }}>
                        <div className='decorator bg-white shadow-md pt-52 px-24 h-full'>
                            <h4 className='text-3xl OpenSans_Bold text-gray-800 mb-3'>Login to your account</h4>
                            <p className='text-[#545454] OpenSans_Regular mt-3 mb-10'>Enter your registered phone number</p>
                            <div className='flex items-center mb-14'>
                                <div className='flex items-center'>
                                    <div className='w-10 h-[40px]'>
                                        <Image className='rounded-full' src={require("../../../../public/assets/images/flag.png")} width={95} height={45} alt='flag' />
                                    </div>
                                    <div className='w-6'>
                                        <Image src={require("../../../../public/assets/images/expand.svg").default} width={95} height={45} alt='expand' />
                                    </div>
                                </div>
                                <div className='flex items-center gap-2 border-[1px] border-solid border-zinc-900 py-3 px-4 w-[85%] rounded-lg'>
                                    {/* <div className='w-4'>
                                        <Image src={require("../../../../public/assets/images/phone.svg").default} width={95} height={45} alt='phone' />
                                    </div> */}
                                    <h5 className='text-sm OpenSans_SemiBold'>+91</h5>
                                    <input type="tel" id='phone' placeholder='Enter your phone number' maxLength={10} className='w-full' />
                                </div>
                            </div>
                            <Link href="#"
                                onClick={() => {
                                    setIsPasswordModal(!isPasswordModal)
                                }}
                                className='py-3 px-48 bg-[#5cc66a] text-xl OpenSans_Bold text-white rounded-lg'>Next</Link>
                            <div className='flex text-center justify-center my-8 gap-2 mb-36'>
                                <h6 className='OpenSans_Medium text-gray-800'>New to Steyp?</h6>
                                <Link href="#" className='OpenSans_Bold text-[#5cc66ad9]'>Create an account</Link>
                            </div>
                            <hr className='border-solid border-[#a5a5a5] mb-5' />
                            <div className='text-center'>
                                <Link href="#" className='OpenSans_Medium text-gray-800'>Terms of service</Link>
                            </div>
                        </div>
                    </div>
                    {isPasswordModal && (
                        <div className='modal'>
                            <div className="modal-content">
                                <Password />
                            </div>
                        </div>
                    )}
                </div>
            )}
        </>
    )
}

export default LoginPage
