"use client"
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'

const FreeTrail = () => {
    const [FreeTrailModal, setFreeTrailModal] = useState(true);
    const handleClose = () => {
        setFreeTrailModal(false);
    };
    const [value, setValue] = useState()
    return (
        <>
            {FreeTrailModal && (
                <div className='inset-0 flex items-center justify-end fixed bg-black bg-opacity-70 z-[1]'>
                    <div className='w-10 flex justify-end  absolute top-8 left-[56%]'onClick={handleClose}>
                        <a href="#">
                            <Image src={require("../../../../public/assets/images/cancel.svg").default} width={96} height={45} alt='cancel' />
                        </a>
                    </div>
                    <div className='h-full'
                        style={{
                            animation: "slideIn 0.5s forwards",
                            transformOrigin: "right",
                            textAlign: "left",
                        }}>
                        <div className='decorator bg-white shadow-md pt-52 px-16 h-full absolute w-[41%] right-0'>
                            <h4 className='text-[28px] OpenSans_Bold text-gray-800 mb-3 w-[98%]'>Steyp is an EdTech company, a beginning of the Digital University for Industry 4.0.</h4>
                            <p className='text-[#545454] OpenSans_Regular mt-3 mb-10'>A digital space for students to learn and become the future Engineers and Scientists. Steyp is looking for capable students who stands out, thinks differently, and keeps a spark to shine!</p>
                            <div className='flex items-center mb-14'>
                                <div className='flex items-center'>
                                    <div className='w-10 h-[40px]'>
                                        <Image className='rounded-full' src={require("../../../../public/assets/images/flag.png")} width={95} height={45} alt='flag' />
                                    </div>
                                    <div className='w-6'>
                                        <Image src={require("../../../../public/assets/images/expand.svg").default} width={95} height={45} alt='expand' />
                                    </div>
                                </div>
                                <div className='flex items-center gap-2 border-[1px] border-solid border-zinc-900 py-3 px-4 w-[85%] rounded-lg'>
                                    <div className='w-4'>
                                        <Image src={require("../../../../public/assets/images/phone.svg").default} width={95} height={45} alt='phone' />
                                    </div>
                                    <h5 className='text-xl OpenSans_SemiBold'>+91</h5>
                                    <input type="tel" id='phone' placeholder='Enter your phone number' maxLength={10} className='w-full'/>
                                </div>
                            </div>
                            <div className='py-3 px-36 now w-full rounded-lg text-center'>
                                <Link href="#" className='text-xl OpenSans_Bold text-white'>Start free trial now!</Link>
                            </div>
                            <div className='flex text-center justify-center my-8 gap-2 mb-36'>
                                <h6 className='OpenSans_Medium text-gray-800'>Already have an account?</h6>
                                <Link href="#" className='OpenSans_Bold text-[#5cc66ad9]'>Login</Link>
                            </div>
                            <hr className='border-solid border-[#a5a5a5] mb-5'/>
                            <div className='text-center'>
                                <Link href="#" className='OpenSans_Medium text-gray-800'>Terms of service</Link>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

export default FreeTrail
