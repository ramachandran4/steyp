"use client"
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'
import LoginPage from '@/app/auth/_compontents/LoginPage'
import FreeTrail from '@/app/auth/_compontents/FreeTrail'
type HeaderProps = {
    isLogin: Boolean;
}

function Header({ isLogin }: HeaderProps) {
    const [isLoginModal, setIsLoginModal] = useState(false);
    const [isFreeTrailModal, setIsFreeTrailModal] = useState(false);
    return ( 
        <div className='py-14'>
            <div className='wrapper'>
                <div className='flex justify-between items-center w-full'>
                    <div className='flex items-center gap-10'>
                        <h1 className='w-32 '>
                            <Link href="/">
                                <Image src={require("../../../public/assets/images/steyp-logo-new.svg").default} width={95} height={44} alt='logo' />
                            </Link>
                        </h1>
                        <Link href="#" className='OpenSans_SemiBold text-[#707070d9]'>School Students</Link>
                        <Link href="#" className='OpenSans_SemiBold text-[#707070d9]'>College Students</Link>
                        <Link href="#" className='OpenSans_SemiBold text-[#707070d9]'>Success stories</Link>
                    </div>
                    <div className='flex justify-between gap-6 items-center'>
                        <Link href="#" onClick={() => {
                            setIsLoginModal(!isLoginModal)
                        }}
                        className='py-4 px-8 border-2 border-solid border-[#4eaf7cd9] rounded-lg OpenSans_SemiBold text-base text-[#4eaf7cd9]'>Go to Dashboard< /Link>
                        <Link href="#" onClick={() => {
                            setIsFreeTrailModal(!isFreeTrailModal)
                        }} className='py-4 px-8 bg-green-600 w-48 text-center free text-white rounded-lg OpenSans_Bold'>Free Trail</Link>
                    </div>
                </div>
            </div>
            {isLoginModal && (
                <div className="modal">
                    <div className="modal-content">
                        <LoginPage />
                    </div>
                </div>
            )}
            {isFreeTrailModal && (
                <div className="modal">
                    <div className="modal-content">
                        <FreeTrail />
                    </div>
                </div>
            )}
        </div>
    )
}

export default Header
